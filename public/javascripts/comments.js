$(document).ready(function(){
    $("#serialize").click(function(){
        var myobj = {Name:$("#Name").val(),Comment:$("#Comment").val()};
        jobj = JSON.stringify(myobj);
        $("#json").text(jobj);
		var url = "comment";
		$.ajax({
			url:url,
			type: "POST",
			data: jobj,
			contentType: "application/json; charset=utf-8",
			success: function(data,textStatus) {
				$("#done").html(textStatus);
			}
		})
    });
	$("#getThem").click(function() {
		$.getJSON('comments', function(data) {
	        console.log(data);
	        var everything = "<ul>";
	        for(var comment in data) {
	        	com = data[comment];
	        	everything += "<li>Name: " + com.Name + " -- Comment: " + com.Comment + "</li>";
	        }
	        everything += "</ul>";
	        $("#comments").html(everything);
	    })
    })
	$("#getCount").click(function(){
		var name = $("#countName").val();
		var url = "../count?name="+name;
		$.getJSON(url, function(count){
			console.log(count);
			var countHTML = "<p>Number of comments from "+name+": "+count+"</p>";
			$("#count").html(countHTML);
		});
		
	})
});
